<?php

namespace Secursus;

use Exception;
use GuzzleHttp\Client;

class Api
{
    const BASE_URL = 'https://developer.secursus.com/api/';
    const ERROR_URI_FIELD_FORMAT = 'Your Api URI must be an string.';
    const ERROR_PARAMS_FIELD_FORMAT = 'Your Api parameters must be an array.';
    const ERROR_METHOD_FIELD_FORMAT = 'Only methods allowed is "GET", "POST" or "DELETE" for the Api request.';
    const ERROR_ID_FIELD_FORMAT = 'Your parcels id must be a string.';
    const ERROR_DATA_FIELD_FORMAT = 'Your arguments data must be an array.';
    const ERROR_AMOUNT_FIELD_FORMAT = 'Your parcels value must be a integer.';
    const ERROR_UNKNOW_API_ID_FIELD_FORMAT = 'The Api ID is required.';
    const ERROR_UNKNOW_API_KEY_FIELD_FORMAT = 'The Api Key is required.';

    /**
     * @var string
     */
    private $api_id;

    /**
     * @var string
     */
    private $api_key;

    /**
     * @var array
     */
    private $available_method = ['GET', 'POST', 'DELETE'];

    /**
     * Api constructor.
     *
     * @param $api_id
     * @param $api_key
     * @throws Exception
     */
    public function __construct($api_id = '', $api_key = '')
    {
        if (empty($api_id)) {
            throw new Exception(self::ERROR_UNKNOW_API_ID_FIELD_FORMAT);
        }

        if (empty($api_key)) {
            throw new Exception(self::ERROR_UNKNOW_API_KEY_FIELD_FORMAT);
        }

        $this->api_id = $api_id;
        $this->api_key = $api_key;

        $this->checkAuthentification();
    }

    /**
     * Cancel parcel order
     *
     * @param $id
     * @return string
     * @throws Exception
     */
    public function cancelParcelOrder($id)
    {
        if (!is_string($id)) {
            throw new Exception(self::ERROR_ID_FIELD_FORMAT);
        }

        return $this->sendRequest('parcels/' . $id . '/delete', [], 'DELETE');
    }

    /**
     * Create parcel order
     *
     * @param $data
     * @return string
     * @throws Exception
     */
    public function createParcelOrder($data)
    {
        if (!is_array($data)) {
            throw new Exception(self::ERROR_DATA_FIELD_FORMAT);
        }

        return $this->sendRequest('parcels/new', $data, 'POST');
    }

    /**
     * Get the insurance Amount
     *
     * @param integer $amount
     * @return string
     * @throws Exception
     */
    public function getInsuranceAmount($amount)
    {
        if (!is_int($amount)) {
            throw new Exception(self::ERROR_AMOUNT_FIELD_FORMAT);
        }

        return $this->sendRequest('parcels/price', ['parcel_value' => $amount], 'POST');
    }

    /**
     * Retrieve parcel order details
     *
     * @param $id
     * @return string
     * @throws Exception
     */
    public function retrieveParcelOrder($id)
    {
        if (!is_string($id)) {
            throw new Exception(self::ERROR_ID_FIELD_FORMAT);
        }

        return $this->sendRequest('parcels/' . $id, [], 'GET');
    }

    /**
     * Retrieve Current Report
     *
     * @return string
     * @throws Exception
     */
    public function retrieveCurrentReport()
    {
        return $this->sendRequest('parcels', [], 'GET');
    }

    /**
     * Retrieve History Report
     *
     * @return string
     * @throws Exception
     */
    public function retrieveHistoryReport()
    {
        return $this->sendRequest('parcels/all', [], 'GET');
    }

    /**
     * Send request to Api
     *
     * @param $uri
     * @param array $params
     * @param string $method
     * @return string
     * @throws Exception
     */
    public function sendRequest($uri, $params = [], $method = 'GET')
    {
        if (!is_string($uri)) {
            throw new Exception(self::ERROR_URI_FIELD_FORMAT);
        }

        if (!is_array($params)) {
            throw new Exception(self::ERROR_PARAMS_FIELD_FORMAT);
        }

        if (!is_string($method) || !in_array($method, $this->available_method)) {
            throw new Exception(self::ERROR_METHOD_FIELD_FORMAT);
        }

        $client = new Client([
            'base_url' => self::BASE_URL
        ]);

        $request = $client->send(
            $client->createRequest($method, $uri, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-type' => 'application/json',
                ],
                'exceptions' => false,
                'auth' => [
                    $this->api_id,
                    $this->api_key
                ],
                'query' => $params
            ])
        );
        $request = json_decode((string) $request->getBody());

        if ($request->response->success) {
            return $request;
        }

        throw new Exception($request->response->detail);
    }

    /**
     * Check Api authentification
     *
     * @return string
     * @throws Exception
     */
    private function checkAuthentification()
    {
        return $this->sendRequest('auth');
    }
}
